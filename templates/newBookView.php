<html>
  <head>
    <title> Add new Book </title>
  </head>
  <body>
  <form action="http://local.rest.com/api/v1/book" method="post">
    <table cellspacing="0">
      <tr>
        <td>
          <label for="title">Title: </label>
        </td>
        <td>
          <input type="text" name="title" id="title" value="">
        </td>
      </tr>
      <tr>
        <td>
          <label for="author">Author: </label>
        </td>
        <td>
          <input type="text" name="author" value="" id="author">
        </td>
      </tr>
      <tr>
        <td>
          <label for="summary">Summary: </label>
        </td>
        <td>
          <textarea name="summary" id="summary"></textarea>
        </td>
      </tr>
      <tr>
        <td>
          <input type="submit" value="Add book">
        </td>
      </tr>
    </table>
  </form>
  <a href="http://local.rest.com/">Home</a>
  </body>
</html>
