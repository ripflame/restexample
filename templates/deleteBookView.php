<?php
require 'vendor/autoload.php';
use GuzzleHttp\Client;

$client = new Client();

if ( isset( $_POST['id'] ) ) {
  $id = $_POST['id'];
  $response = $client->delete( 'http://local.rest.com/api/v1/book/' . $id );

  if ( $response->getStatusCode() ) {
    echo 'The book has been deleted.';
  }
}

$booksResponse = $client->get( 'http://local.rest.com/api/v1/books' );
$books = $booksResponse->json();
?>
<html>
  <head>
    <title>Delete a book</title>
  </head>
  <body>
    <ul>
      <?php foreach ( $books as $book ) : ?>
      <li>
      <?php echo $book['id'];?> <a href="http://local.rest.com/api/v1/book/<?php echo $book['id'];?>"><?php echo $book['title']; ?></a>
      </li>
      <?php endforeach; ?>
    </ul>
    <form action="http://local.rest.com/book/delete" method="post">
      <table cellspacing="0">
        <tr>
          <td>
            <label for="id">Book ID </label>
          </td>
          <td>
            <input type="text" id="id" value="" name="id">
          </td>
        </tr>
        <tr>
          <td>
            <input type="submit" name="submit" value="Delete">
          </td>
        </tr>
      </table>
    </form>
    <a href="http://local.rest.com">Home</a>
  </body>
</html>
