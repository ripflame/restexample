<?php
require 'vendor/autoload.php';
use GuzzleHttp\Client;

$client = new Client();
$response = $client->get( 'http://local.rest.com/api/v1/books' );
$books = $response->json();
?>
<html>
  <head>
    <title>Books</title>
  </head>
  <body>
    <h1>This is the list of all books.</h1>
    <ul>
      <?php foreach ( $books as $book ) : ?>
      <li>
        <a href="http://local.rest.com/book/<?php echo $book['id']; ?>"><?php echo $book['title']; ?></a>
      </li>
      <?php endforeach; ?>
    </ul>
    <hr>
    <a href="http://local.rest.com/book/new">Add new book</a>
    <br>
    <a href="http://local.rest.com/book/delete">Delete a book by ID</a>
  </body>
</html>
