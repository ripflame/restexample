<?php
require_once "api/v1/NotORM.php";

$dsn      = "mysql:dbname = restExample;host = localhost";
$username = "rest";
$password = "rest";

$pdo = new PDO( $dsn, $username, $password );
$db  = new NotORM( $pdo );

require 'api/v1/Slim/Slim.php';
\Slim\Slim::registerAutoloader();

$app = new \Slim\Slim( array(
  "MODE"           => "development",
  "TEMPLATES.PATH" => "./templates"
) );

// add new Route
$app->get("/", function() use ( $app ) {
  $app->render( "home.php" );
});

$app->get( "/book/testing", function() use ( $app ) {
  $app->render( "testing.php" );
});

$app->get("/book/delete", function() use( $app ) {
  $app->render( "deleteBookView.php" );
});

$app->post("/book/delete", function() use( $app ) {
  $app->render( "deleteBookView.php" );
});

$app->get("/book/new", function() use( $app ) {
  $app->render( "newBookView.php" );
});

$app->get( "/book/:id", function ( $id ) use ($app, $db ) {
  $request = $db->books()->where( "id", $id );

  if ( $data = $request->fetch() ) {
    $book = array(
      "id"      => $data['id'],
      "title"   => $data['title'],
      "author"  => $data['author'],
      "summary" => $data['summary']
    );
    $app->render( "bookView.php", array( 'book' => $book ) );
  }
});

$app->get("/book/edit/:id", function( $id ) use ( $app, $db ) {
  $request = $db->books()->where( "id", $id );

  if ( $data = $request->fetch() ) {
    $book = array(
      "id"      => $data['id'],
      "title"   => $data['title'],
      "author"  => $data['author'],
      "summary" => $data['summary']
    );
    $app->render( "bookEdit.php", array( 'book' => $book ) );
  }
});

$app->run();
?>
