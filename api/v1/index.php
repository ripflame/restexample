<?php
require_once "NotORM.php";

$dsn      = "mysql:dbname = restExample;host = localhost";
$username = "rest";
$password = "rest";

$pdo = new PDO( $dsn, $username, $password );
$db  = new NotORM( $pdo );

/**
 * Step 1: Require the Slim Framework
 *
 * If you are not using Composer, you need to require the
 * Slim Framework and register its PSR-0 autoloader.
 *
 * If you are using Composer, you can skip this step.
 */
require 'Slim/Slim.php';

\Slim\Slim::registerAutoloader();

/**
 * Step 2: Instantiate a Slim application
 *
 * This example instantiates a Slim application using
 * its default settings. However, you will usually configure
 * your Slim application now by passing an associative array
 * of setting names and values into the application constructor.
 */
$app = new \Slim\Slim( array(
  "MODE"           => "production",
  "TEMPLATES.PATH" => "./templates"
) );

// add new Route
$app->get("/", function() {
  echo "<h1>REST Service for Books</h1>";
});

// Get all books
$app->get("/books", function() use ( $app, $db ) {
  $books = array();

  foreach ( $db->books() as $book ) {
    $books[] = array(
      "id"      => $book["id"],
      "title"   => $book["title"],
      "author"  => $book["author"],
      "summary" => $book["summary"]
    );
  }

  $app->response()->header( "Content-Type", "application/json" );
  echo json_encode( $books );
});

// Get a book by ID
$app->get("/book/:id", function( $id ) use ( $app, $db ) {
  $app->response()->header( "Content-Type", "application/json" );
  $book = $db->books()->where( "id", $id );

  if ( $data = $book->fetch() ) {
    echo json_encode( array(
      "id"      => $data["id"],
      "title"   => $data["title"],
      "author"  => $data["author"],
      "summary" => $data["summary"]
    ));
  } else {
    echo json_encode( array(
      "status"  => false,
      "message" => "Book ID $id does not exist"
    ));
  }
});

// Add a new book
$app->post("/book", function() use ( $app, $db ) {
  $book   = $app->request()->post();
  $result = $db->books->insert($book);
  echo "Book added";
  echo "<br><a href='http://local.rest.com'>Home</a>";
});

// Update a book
$app->put( "/book/:id", function( $id ) use( $app, $db ) {
  $app->response()->header( "Content-Type", "application/json" );
  $book = $db->books()->where( "id", $id );
  if ( $book->fetch() ) {
    $post   = $app->request()->put();
    $result = $book->update( $post );
    echo json_encode( array(
      "status"  => (bool)$result,
      "message" => "Book updated succesfully"
    ));
  } else {
    echo json_encode( array(
      "status"  => false,
      "message" => "Book id $id does not exist"
    ));
  }
});

// Delete a book
$app->delete( "/book/:id", function( $id ) use ( $app, $db ) {
  $app->response( "Content-Type", "application/json" );
  $book = $db->books()->where( "id", $id );
  if ( $book->fetch() ) {
    $result = $book->delete();
    echo json_encode( array(
      "status"  => true,
      "message" => "Book deleted successfully"
    ));
  } else {
    echo json_encode( array(
      "status"  => false,
      "message" => "Book id $id does not exist"
    ));
  }
});

/**
 * Step 4: Run the Slim application
 *
 * This method should be called last. This executes the Slim application
 * and returns the HTTP response to the HTTP client.
 */
$app->run();
