<html>
  <head>
    <title>View Book</title>
  </head>
  <body>
    <h1><?php echo $book['title']; ?></h1>
    <table cellspacing="0">
      <tr>
        <td>
          <h3>Author: </h3>
        </td>
        <td><h3><?php echo $book['author']; ?></h3></td>
      </tr>
      <tr>
        <td>
          <h3>Summary: </h3>
        </td>
        <td><h3><?php echo $book['summary']; ?></h3></td>
      </tr>
    </table>
    <a href="http://local.rest.com/book/edit/<?php echo $book['id']; ?>">Edit</a>
    <a href="http://local.rest.com/">Home</a>
  </body>
</html>
